# Switcheroo

This add-on is intended to be used in Firefox profiles that are being used for apps/PWAs.
This provides something slightly closer to an "app" feel, where the links you click on open in your regular browser.

It works by detecting links going outside of the current domain, and redirecting them to open in your OS's default browser.

![Screenshot of the Switcheroo menu](/assets/screenshot01.png "Switcheroo")

## How to install
1. Install the Firefox extension.
2. Either run the below command (as your user, not as root) from within the project directory,
   ```
   make install
   ```
   or manually copy the files from `backend` into `~/.mozilla/native-messaging-hosts` and update `switcheroo.json` with the absolute path to `switcheroo.py`.
