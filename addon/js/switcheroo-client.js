const HOSTNAME = window.location.hostname;
const closeDialog = () => { document.getElementById("switcheroo-dialog").close(); };

const dialog = document.createElement("dialog");
document.body.appendChild(dialog);
dialog.outerHTML = `
	<dialog id="switcheroo-dialog">
		<button id="switcheroo-openinbrowser">${browser.i18n.getMessage("openInBrowser")}</button>
		<button id="switcheroo-openhere">${browser.i18n.getMessage("openHere")}</button>
		<button id="switcheroo-cancel">${browser.i18n.getMessage("cancel")}</button>
	</dialog>
`;

document.getElementById("switcheroo-openinbrowser").addEventListener("click", () => {
	browser.runtime.sendMessage(window.switcherooUrl);
	closeDialog();
});
document.getElementById("switcheroo-openhere").addEventListener("click", () => {
	window.open(window.switcherooUrl, "_blank");
	closeDialog();
});
document.getElementById("switcheroo-cancel").addEventListener("click", () => {
	closeDialog();
});

document.addEventListener("click", event => {
	const a = event.target.tagName === "A" ? event.target : event.target.closest("a");
	if (a && a.hostname !== HOSTNAME) {
		event.preventDefault();
		event.stopPropagation();
		event.stopImmediatePropagation();

		const dialog = document.getElementById("switcheroo-dialog");
		window.switcherooUrl = a.href;
		dialog.showModal();
	}
}, { capture: true });
